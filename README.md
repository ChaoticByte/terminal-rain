# Terminal Rain

![](https://gitlab.com/W13R/terminal-rain/-/raw/main/terminal-rain-showcase.webp)

Rain in the linux terminal - written in python3.  
The terminal emulator has to support ANSI escape codes.

## Usage

Run the .py script files with python3:

```
python3 rain.py
python3 colorful-rain.py
python3 colorful-rain.py b
```

Alternatively, on most linux distros, you can run the scripts with:

```
./<scriptname>.py
```

Tip: Use a GPU-accelerated terminal emulator like [alacritty](https://github.com/alacritty/alacritty) for better performance.
